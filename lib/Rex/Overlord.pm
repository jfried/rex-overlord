package Rex::Overlord;
use Mojo::Base 'Mojolicious';

use Rex::Overlord::Schema;
use Rex::Overlord::Mojolicious::Command::ca;
use Rex::Overlord::Mojolicious::Command::cert;

use Rex::Overlord::Repository::Impl::Store::Default;
use Rex::Overlord::Repository::Impl::Rexfile::Default;

has schema => sub {
  my ($self) = @_;

  my $dsn =
      "dbi:Pg:"
    . "database="
    . $self->config->{database}->{schema} . ";" . "host="
    . $self->config->{database}->{host};

  return Rex::Overlord::Schema->connect(
    $dsn,
    $self->config->{database}->{username},
    $self->config->{database}->{password},
  );
};

sub startup {
  my $self = shift;

  #######################################################################
  # Load configuration
  #######################################################################
  my @cfg = (
    "overlord.conf", "/etc/rex/overlord.conf",
    "/usr/local/etc/rex/overlord.conf",
  );
  my $cfg;
  for my $file (@cfg) {
    if ( -f $file ) {
      $cfg = $file;
      last;
    }
  }

  #######################################################################
  # Load plugins
  #######################################################################
  $self->plugin( "Config", file => $cfg );
  $self->helper( db => sub { $self->app->schema } );
  $self->helper(
    repository => sub {
      return Rex::Overlord::Repository::Impl::Store::Default->new(
        app => $self->app,
        db  => $self->db
      );
    }
  );

  #######################################################################
  # Router
  #######################################################################
  my $r = $self->routes;

  $r->get('/')->to('main#index');

  $r->post('/1.0/broker/queue')->to('broker#queue');
  $r->post('/1.0/ca/csr')->to('ca#upload_csr');
  $r->get('/1.0/ca')->to('ca#get_ca');
  $r->get('/1.0/ca/cert/#cn')->to('ca#get_cert_of_cn');

  $r->post('/1.0/rex/:rexfile/run/:task/#host')->to('rex#run');
  $r->post('/1.0/rex/:rexfile/#version/upload')->to('rex#rexfile_upload');

  $r->websocket('/1.0/broker')->to('broker#index');
}

1;

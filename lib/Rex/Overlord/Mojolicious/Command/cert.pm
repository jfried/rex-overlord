package Mojolicious::Command::cert;

use Mojo::Base 'Mojolicious::Command';
use Expect;
use Data::Dumper;
use Sys::Hostname;
use File::Path 'make_path';
use Carp;

sub run {
  my ( $self, $command, @args ) = @_;

  my $ssl_base = $self->app->config->{ssl}->{dir} . "/ca";

  if ( $command eq "list" ) {
    my @certs;

    print "\n## Legend\n\n";
    print "[P] - pending certificates (waiting for approval)\n";
    print "[S] - signed certificates\n\n";
    print "## Certificates\n\n";

    if ( $args[0] && ( $args[0] eq "--waiting" || $args[0] eq "--all") ) {
      opendir( my $dh, "$ssl_base/csr" ) or die($!);
      while ( my $entry = readdir($dh) ) {
        next if ( $entry !~ m/\.csr/ );
        my ($name) = ($entry =~ m/^(.*)\..+?/);
        print "[P] $name\n";
      }
      closedir($dh);
    }

    if( ! $args[0] || ( $args[0] && $args[0] eq "--all" ) ) {
      opendir( my $dh, "$ssl_base/certs" ) or die($!);
      while ( my $entry = readdir($dh) ) {
        next if ( $entry !~ m/\.crt/ );
        next if ( $entry eq "ca.crt" );
        my ($name) = ($entry =~ m/^(.*)\..+?/);
        
        print "[S] $name\n";
      }
      closedir($dh);
    }
  }

  elsif($command eq "sign") {
    my $what = $args[0];

    if($what) {
      if(-f "$ssl_base/csr/$what.csr") {
        chdir $ssl_base;

        my $cmd = qq{echo -e "y\ny" | openssl ca -config conf/openssl.cnf -policy signing_policy -extensions signing_req -out certs/$what.crt -infiles csr/$what.csr};
        $self->app->log->debug("Running $cmd");

        my $out = qx{$cmd};
        if($? != 0) {
          $self->app->log->error("Can't sign certificate.");
          $self->app->log->error($out);
          confess "Can't sign certificate.";
        }

        unlink "csr/$what.csr";
      }
      else {
        $self->app->log->error("Can't find certificate request for $what.");
        exit 1;
      }
    }
    else {
      $self->app->log->error("No certificate request given.");
      exit 1;
    }
  }
}

1;

package Mojolicious::Command::ca;

use Mojo::Base 'Mojolicious::Command';
use Expect;
use Data::Dumper;
use Sys::Hostname;
use File::Path 'make_path';

sub run {
  my ( $self, $command, %args ) = @_;

  if($command eq "create") {

    my $param = \%args;

    # defaults
    $param->{password} ||= "";
    $param->{country}  ||= "XX";
    $param->{state}    ||= "State";
    $param->{city}     ||= "City";
    $param->{org}      ||= "Rex Swarm";
    $param->{unit}     ||= "Rex Overlord";
    $param->{cn}       ||= hostname() . " CN";
    $param->{email}    ||= "hostmaster\@" . hostname();

    # get the default ssl conf out of the <DATA> store
    my @openssl_cnf = <DATA>;
    chomp @openssl_cnf;

    # create the files and directories

    my $ssl_base = $self->app->config->{ssl}->{dir};
    make_path "$ssl_base/ca";
    make_path "$ssl_base/ca/conf";
    make_path "$ssl_base/ca/private";
    make_path "$ssl_base/ca/csr";
    make_path "$ssl_base/ca/tmp";
    make_path "$ssl_base/ca/certs";
    make_path "$ssl_base/ca/public";

    chdir "$ssl_base/ca";

    open( my $fh, ">", "conf/openssl.cnf" ) or die($!);
    print $fh join( "\n", @openssl_cnf );
    close($fh);

    chmod 0600, "conf/openssl.cnf";
    chmod 0700, "private";

    open( $fh, ">", "index.txt" );
    close($fh);

    open( $fh, ">", "serial" );
    print $fh "01\n";
    close($fh);

    my $cmd =
      "LC_ALL=C openssl req -nodes -config conf/openssl.cnf -new -x509 -keyout private/ca.key -out certs/ca.crt -days 7300";
    my $exp = Expect->spawn($cmd);
    $exp->expect(
      5,
      [
        qr/Enter PEM pass phrase/ => sub {
          my $_exp = shift;
          $_exp->send( $param->{password} . "\n" );
          exp_continue;
        }
      ],
      [
        qr/Country Name/ => sub {
          my $_exp = shift;
          $_exp->send( $param->{country} . "\n" );
          exp_continue;
        }
      ],
      [
        qr/State or Province Name/ => sub {
          my $_exp = shift;
          $_exp->send( $param->{state} . "\n" );
          exp_continue;
        }
      ],
      [
        qr/Locality Name/ => sub {
          my $_exp = shift;
          $_exp->send( $param->{city} . "\n" );
          exp_continue;
        }
      ],
      [
        qr/Organization Name/ => sub {
          my $_exp = shift;
          $_exp->send( $param->{org} . "\n" );
          exp_continue;
        }
      ],
      [
        qr/Organizational Unit Name/ => sub {
          my $_exp = shift;
          $_exp->send( $param->{unit} . "\n" );
          exp_continue;
        }
      ],
      [
        qr/Common Name/ => sub {
          my $_exp = shift;
          $_exp->send( $param->{cn} . "\n" );
          exp_continue;
        }
      ],
      [
        qr/Email Address/ => sub {
          my $_exp = shift;
          $_exp->send( $param->{email} . "\n" );
          exp_continue;
        }
      ],
    );

    $exp->soft_close;
  }

}

1;

__DATA__
[ ca ]
default_ca= myca # The default ca section
#################################################################

[ myca ]
dir=.

certs=$dir/certs # Where the issued certs are kept
crl_dir= $dir/crl # Where the issued crl are kept
database= $dir/index.txt # database index file
new_certs_dir= $dir/certs # default place for new certs
certificate=$dir/certs/ca.crt # The CA certificate
serial= $dir/serial # The current serial number
crl= $dir/crl.pem # The current CRL
private_key= $dir/private/ca.key # The private key
RANDFILE= $dir/.rand # private random number file
default_days= 365 # how long to certify for
default_crl_days= 30 # how long before next CRL
default_md= md5 # which message digest to use
preserve= no # keep passed DN ordering

# A few different ways of specifying how closely the request should
# conform to the details of the CA

policy= policy_anything 

[ policy_anything ]
countryName = optional
stateOrProvinceName= optional
localityName= optional
organizationName = optional
organizationalUnitName = optional
commonName= supplied
emailAddress= optional

[ req ]
default_bits = 1024
default_keyfile= privkey.pem
distinguished_name = req_distinguished_name
attributes = req_attributes

[ req_distinguished_name ]
countryName= Country Name (2 letter code)
countryName_min= 2
countryName_max = 2
stateOrProvinceName= State or Province Name (full name)
localityName = Locality Name (eg, city)
organizationName = Organization Name (eg, company)
organizationalUnitName = Organizational Unit Name (eg, section)
commonName = Common Name (eg. YOUR name)
commonName_max = 64
emailAddress = Email Address
emailAddress_max = 40

[ req_attributes ]
challengePassword = A challenge password
challengePassword_min = 0 
challengePassword_max = 20
unstructuredName= An optional company name

[ signing_policy ]
countryName     = optional
stateOrProvinceName = optional
localityName        = optional
organizationName    = optional
organizationalUnitName  = optional
commonName      = supplied
emailAddress        = optional

[ signing_req ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer

basicConstraints = CA:FALSE
keyUsage = digitalSignature, keyEncipherment

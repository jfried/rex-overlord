package Rex::Overlord::Controller::Ca;
use Mojo::Base 'Mojolicious::Controller';
use File::Temp 'tempfile';

sub get_ca {
  my $self     = shift;
  my $ssl_base = $self->config->{ssl}->{dir} . "/ca";

  if ( !-f "$ssl_base/certs/ca.crt" ) {
    return $self->render(
      json   => { ok => Mojo::JSON->false, error => "No ca file found." },
      status => 500
    );
  }

  my $ca_file = eval { local ( @ARGV, $/ ) = ("$ssl_base/certs/ca.crt"); <>; };

  $self->render( json => { ok => Mojo::JSON->true, data => $ca_file } );
}

sub get_cert_of_cn {
  my $self = shift;

  my $ssl_base = $self->config->{ssl}->{dir} . "/ca";
  my $cn       = $self->param("cn");

  if ( !-f "$ssl_base/certs/$cn.crt" ) {
    return $self->render(
      json   => { ok => Mojo::JSON->false, error => "No cert file found." },
      status => 404
    );
  }

  my $crt_file =
    eval { local ( @ARGV, $/ ) = ("$ssl_base/certs/$cn.crt"); <>; };

  $self->render( json => { ok => Mojo::JSON->true, data => $crt_file } );
}

sub upload_csr {
  my $self = shift;

  my $ssl_base = $self->config->{ssl}->{dir} . "/ca";
  my $csr_path = "$ssl_base/csr";

  my ( $t_fh, $t_filename ) = tempfile();
  print $t_fh $self->req->json->{csr};
  print $t_fh "\n";

  my $cn = qx{openssl req -in $t_filename -noout -text | grep CN};
  close $t_fh;

  $cn =~ s/^.*CN=([^\s]+)/$1/;
  chomp $cn;

  $self->app->log->debug("Found certificate request for host: $cn");
  $self->app->log->debug("Writing csr to $csr_path/$cn.csr");

  open( my $csr, ">", "$csr_path/$cn.csr" ) or die($!);
  print $csr $self->req->json->{csr} . "\n";
  close $csr;

  $self->render( json => { ok => Mojo::JSON->true } );
}

1;

package Rex::Overlord::Controller::Broker;
use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;

our @clients;

my $command_check = 0;

sub queue {
  my $self = shift;
  my $ref  = $self->req->json;

  $self->app->log->debug( "Queuing message: " . Dumper($ref) );
  eval {
    $self->db->resultset("BrokerMessage")->create($ref);

    $self->render(
      json => { ok => Mojo::JSON->true, message => "Command queued." } );
    1;
  } or do {
    $self->app->log->error($@);
    $self->render(
      json   => { ok => Mojo::JSON->false, error => $@ },
      status => 500
    );
  };

}

sub index {
  my $self = shift;

  my $client_ip = $self->tx->remote_address;

  push @clients, { ip => $client_ip, tx => $self->tx, };

  $self->on(
    json => sub {
      my ( $self, $data ) = @_;
      $self->app->log->debug( "Git json message: " . Dumper($data) );
    },
  );

  $self->on(
    finish => sub {
      my ($self) = @_;
      $self->app->log->debug(
        "Connection closed by: " . $self->tx->remote_address );
      @clients = grep { $_->{ip} ne $self->tx->remote_address } @clients;

      if ( scalar @clients == 0 ) {
        Mojo::IOLoop->remove($command_check);
        $command_check = 0;
      }
    },
  );

  if ( !$command_check ) {
    $command_check = Mojo::IOLoop->recurring(
      3 => sub {
        $self->app->log->debug("Checking for commands...");

        my $messages = $self->db->resultset("BrokerMessage")->search( {} );
        while ( my $message = $messages->next ) {
          $self->app->log->debug(
            "Found message for: " . $message->for_system );
          my $ip = $message->for_system;
          my ($client) = grep { $_->{ip} eq $ip } @clients;
          if ($client) {
            $client->{tx}->send( { json => $message->data } );
            $message->delete();
          }
        }

      }
    );
  }

}

1;

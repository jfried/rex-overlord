package Rex::Overlord::Controller::Main;
use Mojo::Base 'Mojolicious::Controller';

sub index {
  my $self = shift;
  $self->render( json => { ok => Mojo::JSON->true } );
}

1;

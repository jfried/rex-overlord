package Rex::Overlord::Controller::Rex;
use Mojo::Base 'Mojolicious::Controller';
use File::Temp 'tempfile';
use File::Spec;
use File::Path qw'make_path remove_tree';
use File::chdir;
use YAML;

sub run {
  my $self = shift;
  my $host = $self->param("host");
  my $task = $self->param("task");

  my $ref = $self->req->json;
  my $user = $ref->{user} || $self->config->{rex}->{auth}->{user} || 'root';
  my $auth_type =
    $ref->{auth_type} || $self->config->{rex}->{auth}->{auth_type} || 'pass';
  my $pass = $ref->{password} || $self->config->{rex}->{auth}->{password};
  my $private_key =
    $ref->{private_key} || $self->config->{rex}->{auth}->{private_key};
  my $public_key =
    $ref->{public_key} || $self->config->{rex}->{auth}->{public_key};

  Mojo::IOLoop->stream( $self->tx->connection )->timeout(300);
  $self->res->headers->content_type("text/plain");

  my @rex_cmd = ("rex");

  push @rex_cmd, "-u $user";

  if ( $auth_type eq "pass" ) {
    push @rex_cmd, "-p '$pass'";
  }
  elsif ( $auth_type eq "key" ) {
    push @rex_cmd, "-P $private_key";
    push @rex_cmd, "-K $public_key";
  }

  push @rex_cmd, "-H $host";
  push @rex_cmd, $task;

  my $pid = open( my $cmd, "-|", join( " ", @rex_cmd ) ) or die($!);
  my $stream = Mojo::IOLoop::Stream->new($cmd);

  my $cb = $stream->on(
    read => sub {
      my ( $stream, $chunk ) = @_;
      chomp $chunk;
      $self->write("[$chunk]\n");
    }
  );

  $stream->on( close => sub { $self->finish("\n") } );

  $self->on(
    finish => sub {
      my $self = shift;
      kill 3, $pid;
      $self->rendered(200);
      $stream->close;
      $stream->unsubscribe( read => $cb );
      $stream->stop;
    }
  );

  $stream->start;

}

sub rexfile_upload {
  my $self = shift;

  my $rexfile_archive = $self->param("rexfile_archive");

  if ( $rexfile_archive && $rexfile_archive->filename ) {
    $self->app->log->debug(
      "Upload data found: " . $rexfile_archive->filename );

    if (
      $self->repository->exists(
        $self->param("rexfile"),
        $self->param("version")
      )
      )
    {
      return $self->render(
        json => { ok => Mojo::JSON->false, error => "Rexfile already exists." },
        status => 409
      );
    }

    if ( $self->req->is_limit_exceeded ) {

      # $ENV{MOJO_MAX_MESSAGE_SIZE} = 1024 * 1024 * 1024;
      return $self->render(
        json => {
          ok => Mojo::JSON->false,
          error =>
            "You have reached upload size limit. You can set this limit higher in the configuration file."
        }
      );
    }

    my $rexfile_path = File::Spec->catdir(
      $self->config->{repository}->{path},
      "rexfiles",
      $self->param("rexfile"),
      $self->param("version")
    );
    my $rexfile_file = File::Spec->catfile( $rexfile_path, "data.tar.gz" );
    make_path $rexfile_path;

    $rexfile_archive->move_to($rexfile_file);
    my $meta;

    {
      local $CWD = $rexfile_path;
      $self->app->log->debug("Extracting $rexfile_file");
      my $cmd = "tar xzvf data.tar.gz 2>&1";
      $self->app->log->debug("Running command: $cmd");
      my @out = qx{$cmd};
      chomp @out;
      $self->app->log->debug( join( "\n", @out ) );

      if ( $? != 0 ) {
        return $self->render(
          json => {
            ok    => Mojo::JSON->false,
            error => "Error extracting data archive.\n"
              . join( "\n", @out[ 0 .. 5 ] )
          },
          status => 500,
        );
      }

      $meta = YAML::LoadFile("meta.yml");
    }

    $self->repository->store( $self->param("rexfile"),
      $self->param("version"), $meta );

    return $self->render( json => { ok => Mojo::JSON->true } );

  }
  else {
    return $self->render(
      json   => { ok => Mojo::JSON->false, error => "No upload data found." },
      status => 500
    );
  }
}

1;

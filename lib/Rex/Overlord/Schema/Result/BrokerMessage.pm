#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Overlord::Schema::Result::BrokerMessage;

use strict;
use warnings;

use base qw(DBIx::Class::Core);

__PACKAGE__->load_components( 'InflateColumn::Serializer',
  'InflateColumn::DateTime', 'Core' );

__PACKAGE__->table("broker_messages");
__PACKAGE__->add_columns(
  id => {
    data_type         => 'serial',
    is_auto_increment => 1,
    is_numeric        => 1,
  },
  for_system => {
    data_type   => 'varchar',
    size        => 250,
    is_nullable => 0,
  },
  r_date => {
    data_type     => 'timestamp',
    is_nullable   => 0,
    default_value => \'CURRENT_TIMESTAMP',
  },
  data => {
    data_type        => 'jsonb',
    is_nullable      => 1,
    serializer_class => 'JSON',
  },
);

__PACKAGE__->set_primary_key("id");

1;

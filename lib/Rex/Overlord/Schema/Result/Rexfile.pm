#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Overlord::Schema::Result::Rexfile;

use strict;
use warnings;

use base qw(DBIx::Class::Core);

__PACKAGE__->load_components( 'InflateColumn::Serializer',
  'InflateColumn::DateTime', 'Core' );

__PACKAGE__->table("rexfiles");
__PACKAGE__->add_columns(
  id => {
    data_type         => 'serial',
    is_auto_increment => 1,
    is_numeric        => 1,
  },
  name => {
    data_type   => 'varchar',
    is_nullable => 0,
  },
  version => {
    data_type   => 'double',
    is_nullable => 0,
    is_numeric  => 1,
  },
  data => {
    data_type        => 'jsonb',
    is_nullable      => 1,
    serializer_class => 'JSON',
  },
);

__PACKAGE__->add_unique_constraint( name_and_version => [qw/name version/], );

__PACKAGE__->set_primary_key("id");

1;

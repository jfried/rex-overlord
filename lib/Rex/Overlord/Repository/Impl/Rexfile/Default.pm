#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Overlord::Repository::Impl::Rexfile::Default;

use Moo;
use common::sense;

extends qw(Rex::Overlord::Repository::Object);
with qw(Rex::Overlord::Repository::Rexfile);

sub name {
  return $_[0]->rs->name;
}

sub version {
  return $_[0]->rs->version;
}

sub data {
  return $_[0]->rs->data;
}

1;

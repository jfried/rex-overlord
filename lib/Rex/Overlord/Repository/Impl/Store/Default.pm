#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Overlord::Repository::Impl::Store::Default;

use Moo;
use common::sense;

use Rex::Overlord::Repository::Impl::Rexfile::Default;

extends qw(Rex::Overlord::Repository::Object);
with qw(Rex::Overlord::Repository::Store);

sub store {
  my ( $self, $name, $version, $data ) = @_;

  $self->app->log->debug("Saving new Rexfile: $name ($version).");

  my $obj = $self->db->resultset("Rexfile")->create(
    {
      name    => $name,
      version => $version,
      data    => $data,
    }
  );

  $self->app->log->debug("New Rexfile created with id: " . $obj->id);
  return Rex::Overlord::Repository::Impl::Rexfile::Default->new(rs => $obj);
}

sub retrieve {
  my ($self, $name, $version) = @_;

  my $db_obj = $self->db->resultset("Rexfile")->search({
    name => $name,
    version => $version,
  })->next;

  if($db_obj) {
    my $obj = Rex::Overlord::Repository::Impl::Rexfile::Default->new(rs => $db_obj);
    return $obj;
  }

  return undef;
}

sub exists {
  my ($self, $name, $version) = @_;

  my $db_obj = $self->db->resultset("Rexfile")->search({
    name => $name,
    version => $version,
  })->next;

  if($db_obj) {
    return 1;
  }

  return 0;
}

sub delete {
  my ($self) = @_;
}

sub find {
  my ($self) = @_;
}

1;

#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Overlord::Repository::Rexfile;

use Moo::Role;

requires qw(name version data);

has rs => ( is => 'ro', lazy => 1 );

1;

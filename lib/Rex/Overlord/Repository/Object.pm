#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:
   
package Rex::Overlord::Repository::Object;

use Moo;
use common::sense;

has db  => ( is => 'ro', lazy => 1 );
has app => ( is => 'ro', lazy => 1 );

1;

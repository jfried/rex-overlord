#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Overlord::Repository::Store;

use Moo::Role;

requires qw(store retrieve exists delete find);

1;

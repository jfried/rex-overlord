-- 
-- Created by SQL::Translator::Producer::PostgreSQL
-- Created on Sun May  3 22:57:05 2015
-- 
;
--
-- Table: broker_messages.
--
CREATE TABLE "broker_messages" (
  "id" serial NOT NULL,
  "for_system" character varying(250) NOT NULL,
  "r_date" timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  "data" jsonb,
  PRIMARY KEY ("id")
);

;
--
-- Table: rexfiles.
--
CREATE TABLE "rexfiles" (
  "id" serial NOT NULL,
  "name" character varying NOT NULL,
  "version" numeric NOT NULL,
  "data" jsonb,
  PRIMARY KEY ("id"),
  CONSTRAINT "name_and_version" UNIQUE ("name", "version")
);

;
